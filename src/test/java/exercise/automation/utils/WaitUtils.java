package exercise.automation.utils;

import static exercise.automation.driver.WebDriverManager.getWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WaitUtils
{

	public static int seconds = 30;
	public static int checkSeconds = 3;

	public static void setSeconds(int secs)
	{
		seconds = secs;
	}

	public static void waitForElementPresentById(String id)
	{

		WebDriver driver = getWebDriver();
		for (int x = 1; x <= seconds; x++)
		{
			try
			{
				sleep();
				WebElement element = driver.findElement(By.id(id));
				if (element.isDisplayed() && element.isEnabled())
				{
					return;
				}
			} catch (NoSuchElementException e)
			{
			}
		}
		throw new RuntimeException("Timed out waiting for element with id: '"
				+ id + "'");
	}

	public static boolean isElementPresentById(String id)
	{

		WebDriver driver = getWebDriver();
		for (int x = 1; x <= checkSeconds; x++)
		{
			try
			{
				sleep();
				WebElement element = driver.findElement(By.id(id));
				if (element.isDisplayed() && element.isEnabled())
				{
					return true;
				}
			} catch (NoSuchElementException e)
			{
			}
		}
		return false;
	}

	public static String waitAndReturnAttributeByCSS(String cssExpression,
			String attribute)
	{
		WebDriver driver = getWebDriver();
		for (int x = 1; x <= seconds; x++)
		{
			try
			{
				sleep();
				String issueKey = driver.findElement(
						By.cssSelector(cssExpression)).getAttribute(attribute);
				return issueKey;
			} catch (NoSuchElementException e)
			{
			}
		}
		throw new RuntimeException(
				"Timed out waiting for element with cssSelector: "
						+ cssExpression);
	}

	private static void sleep()
	{
		try
		{
			Thread.sleep(1000);
		} catch (InterruptedException ex)
		{
			ex.printStackTrace();
		}
	}

}
