package exercise.automation.utils;

import static java.util.logging.Level.INFO;

import java.util.logging.Logger;

import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;

import exercise.automation.driver.WebDriverManager;
import exercise.automation.pages.LoggedInHomePage;

public class LocalMatcherAssert
{
	static Logger LOG = Logger
			.getLogger(LoggedInHomePage.class.getSimpleName());

	public static <T> void assertThat(T actual, Matcher<? super T> matcher)
	{
		LOG.log(INFO, "***"
				+ WebDriverManager.getCurrentPage().getClass().getSimpleName()
				+ ": assertThat( " + actual + " , " + matcher + " )");
		MatcherAssert.assertThat(actual, matcher);
	}

}
