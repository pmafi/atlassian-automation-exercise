package exercise.automation.pages;

import static exercise.automation.utils.LocalMatcherAssert.assertThat;
import static java.util.logging.Level.INFO;
import static org.hamcrest.Matchers.containsString;

import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import exercise.automation.jira.Issue;
import exercise.automation.utils.WaitUtils;

public class LoggedInHomePage extends BasePage
{

	Logger LOG = Logger.getLogger(LoggedInHomePage.class.getSimpleName());

	@FindBy(id = "create_link")
	private WebElement createLink;

	@FindBy(id = "summary")
	private WebElement summaryInput;

	@FindBy(id = "description")
	private WebElement descTxtArea;

	@FindBy(id = "create-issue-submit")
	private WebElement createButton;

	@FindBy(id = "quickSearchInput")
	private WebElement quickSearchInput;

	public LoggedInHomePage(WebDriver driver) {
		super(driver);
	}

	public Issue createIssue(Issue inputIssue)
	{
		String summary = inputIssue.getSummary();
		String description = inputIssue.getDescription();

		create(summary, description);

		String key = WaitUtils.waitAndReturnAttributeByCSS(
				"div.aui-message > a.issue-created-key", "data-issue-key");

		assertThat(key, containsString("TST"));
		LOG.log(INFO, "A new issue has been created: " + key);

		return new Issue(key, summary, description);
	}

	public IssuePage searchForIssue(String search)
	{
		quickSearchInput.sendKeys(search);
		quickSearchInput.submit();

		LOG.log(INFO, "Searching for issue: " + search);

		WaitUtils.waitForElementPresentById("edit-issue");

		assertThat(driver.getTitle(), containsString(search));

		return PageFactory.initElements(driver, IssuePage.class);
	}

	private void create(String summary, String description)
	{
		createLink.click();

		ensureCreateContainerIsLoaded();

		summaryInput.sendKeys(summary);
		descTxtArea.sendKeys(description);
		createButton.click();
	}

	private void ensureCreateContainerIsLoaded()
	{
		if (!WaitUtils.isElementPresentById("summary"))
		{
			createLink.click();
			WaitUtils.waitForElementPresentById("summary");
		}
		LOG.log(INFO, "Issue creation starting...");
	}

}
