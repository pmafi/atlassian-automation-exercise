package exercise.automation.pages;

import static java.util.logging.Level.INFO;

import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;

import exercise.automation.driver.WebDriverManager;

public abstract class BasePage implements BasePageIF
{

	protected WebDriver driver;
	Logger LOG = Logger.getLogger(BasePage.class.getSimpleName());

	public BasePage(WebDriver driver) {
		this.driver = driver;
		WebDriverManager.setCurrentPage(this);
		LOG.log(INFO, "New page title: '" + driver.getTitle() + "'");
	}

}
