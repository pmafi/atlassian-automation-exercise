package exercise.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class JiraLandingPage extends BasePage
{

	@FindBy(linkText = "Log In")
	public WebElement loginLink;

	public JiraLandingPage(WebDriver driver) {
		super(driver);
	}

	public LoginPage gotoLoginPage()
	{
		loginLink.click();
		return PageFactory.initElements(driver, LoginPage.class);
	}

}
