package exercise.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import exercise.automation.utils.WaitUtils;

public class LoginPage extends BasePage
{

	@FindBy(id = "username")
	private WebElement username;

	@FindBy(id = "password")
	private WebElement password;

	@FindBy(id = "login-submit")
	private WebElement button;

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	public LoggedInHomePage login()
	{
		username.sendKeys("patrick.mafi@gmail.com");
		password.sendKeys("Password1");
		button.click();
		WaitUtils.waitForElementPresentById("create-menu");
		return PageFactory.initElements(driver, LoggedInHomePage.class);
	}

}
