package exercise.automation.pages;

import static java.util.logging.Level.INFO;

import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import exercise.automation.jira.Issue;
import exercise.automation.utils.WaitUtils;

public class IssuePage extends BasePage
{

	Logger LOG = Logger.getLogger(IssuePage.class.getSimpleName());

	@FindBy(id = "edit-issue")
	private WebElement editIssueLink;

	@FindBy(id = "summary")
	private WebElement summaryInput;

	@FindBy(id = "description")
	private WebElement descTxtArea;

	@FindBy(id = "summary-val")
	private WebElement savedSummaryText;

	@FindBy(id = "description-val")
	private WebElement savedDescText;

	@FindBy(id = "edit-issue-submit")
	private WebElement updateButton;

	public IssuePage(WebDriver driver) {
		super(driver);
	}

	public Issue editIssue(Issue inputIssue)
	{
		editIssueLink.click();

		ensureEditContainerIsLoaded();

		summaryInput.clear();
		summaryInput.sendKeys(inputIssue.getSummary());

		descTxtArea.clear();
		descTxtArea.sendKeys(inputIssue.getDescription());

		updateButton.click();
		return readIssueFromPage();

	}

	public Issue readIssueFromPage()
	{
		driver.navigate().refresh();
		Issue issue = new Issue("READ_ISSUE", savedSummaryText.getText(),
				savedDescText.getText());

		LOG.log(INFO, "Reading issue page: summary=" + issue.getSummary()
				+ " description=" + issue.getDescription());
		return issue;
	}

	private void ensureEditContainerIsLoaded()
	{
		if (!WaitUtils.isElementPresentById("summary"))
		{
			editIssueLink.click();
			WaitUtils.waitForElementPresentById("summary");
		}
		LOG.log(INFO, "Issue editing starting...");
	}

}
