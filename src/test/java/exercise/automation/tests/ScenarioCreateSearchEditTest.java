package exercise.automation.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import exercise.automation.driver.WebDriverManager;
import exercise.automation.jira.Issue;
import exercise.automation.pages.IssuePage;
import exercise.automation.pages.JiraLandingPage;
import exercise.automation.pages.LoggedInHomePage;
import exercise.automation.pages.LoginPage;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static exercise.automation.utils.LocalMatcherAssert.assertThat;

public class ScenarioCreateSearchEditTest
{

	private JiraLandingPage jiraLandingPage;
	private LoginPage loginPage;
	private LoggedInHomePage homePage;
	private IssuePage issuePage;

	@Before
	public void setUp()
	{
		jiraLandingPage = WebDriverManager.startAFlow();
		loginPage = jiraLandingPage.gotoLoginPage();
		homePage = loginPage.login();
	}

	@After
	public void cleanUp()
	{
		WebDriverManager.getWebDriver().quit();
	}

	@Test
	public void shouldCreateSearchAndEditAnIssue()
	{

		// given input issue
		Issue inputIssue = new Issue("", "Jira Project History Summary",
				"THIS IS NEW ISSUE DESCRIPTION TEXT");

		// when I create the issue
		Issue createdIssue = homePage.createIssue(inputIssue);

		// then
		assertThat(createdIssue.getKey(), containsString("TST"));

		// when i search for issue
		issuePage = homePage.searchForIssue(createdIssue.getKey());

		// when i modify input issue
		inputIssue.appendToSummary("HELLO! - Im edited");
		inputIssue.appendToDescription("HELLO! - Im edited");
		Issue readIssue = issuePage.editIssue(inputIssue);

		// then
		assertThat(readIssue.getSummary(),
				equalToIgnoringWhiteSpace(inputIssue.getSummary()));
		assertThat(readIssue.getDescription(),
				equalToIgnoringWhiteSpace(inputIssue.getDescription()));
	}

}
