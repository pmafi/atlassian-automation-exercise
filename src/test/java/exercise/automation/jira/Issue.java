package exercise.automation.jira;

public class Issue
{

	private String key;
	private String summary;
	private String description;

	public Issue(String key, String summary, String description) {
		this.key = key;
		this.summary = summary;
		this.description = description;
	}

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	public String getSummary()
	{
		return summary;
	}

	public void setSummary(String summary)
	{
		this.summary = summary;
	}

	public void appendToSummary(String summary)
	{
		String buff = this.summary;
		setSummary(buff.concat(" " + summary));
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void appendToDescription(String description)
	{
		String buff = this.description;
		setDescription(buff.concat("\n" + description));
	}

}
