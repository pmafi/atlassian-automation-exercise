package exercise.automation.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import exercise.automation.pages.BasePageIF;
import exercise.automation.pages.JiraLandingPage;

public class WebDriverManager
{

	private static WebDriver driver;
	private static BasePageIF currentPage;

	public static WebDriver getWebDriver()
	{
		if (driver == null)
		{
			throw new RuntimeException(
					"getWebDriver() failed, please create a WebDriver first.");
		}
		return driver;
	}

	public static JiraLandingPage startAFlow()
	{
		driver = new FirefoxDriver();
		driver.get("https://jira.atlassian.com/browse/TST/");
		return PageFactory.initElements(driver, JiraLandingPage.class);
	}

	public static BasePageIF getCurrentPage()
	{
		return currentPage;
	}

	public static void setCurrentPage(BasePageIF currPage)
	{
		currentPage = currPage;
	}

}
