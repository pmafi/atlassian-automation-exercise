Test pre-requisites:
- Windows
- Maven (3.2.3)
- Java (1.7)
- Internet access (public maven repo)
- Access to public jira "https://jira.atlassian.com/browse/TST/"
- firefox browser

I took afew shortucts (for times sake) that i would not normally, some examples:
- I have hard-coded my login in the project.
- I have afew private methods that fix small problems waiting for sections of the page to be acted upon. Should be moved into a more robust base of element interactions so its more reusable.
- include a packged version of firefox in the project for the tests to run against.

How to run:
commandline "mvn clean install" from "atlassian-automation-exercise" directory.